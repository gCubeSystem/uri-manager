package org.gcube.data.access.urimanager;

import static org.junit.Assert.assertEquals;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Application;



public class GreetingTest extends JerseyTest{
	
			 
	    @Override
	    protected Application configure() {
	        return new ResourceConfig(UriManager.class);
	    }
	 
	    @Test
	    public void test() {
	        final String hello = target("hello").request().get(String.class);
	        assertEquals("Hello World!", hello);
	    }
	
}
