package org.gcube.data.access.urimanager;

import org.glassfish.jersey.server.ResourceConfig;

import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("uri-manager")
public class MyApplication extends ResourceConfig{


	public MyApplication() {
        // Resources.
        packages(UriManager.class.getPackage().getName());
	}
}
