package org.gcube.data.access.urimanager;

import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/")
public class UriManager extends Application {

	@PathParam("plugin") String pluginName;
	@PathParam("version") String pluginVersion;
	@PathParam("resource") String resourceId;
	@QueryParam("authorized-access") boolean authorizedAccess = false; 
	
	
	@POST
	@Path("/{name}/{resource}/{version}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response createUriWithVersion() {
			return null;	
	}
	
	@POST
	@Path("/{name}/{resource}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response createUri() {
		return Response.ok("Hello " +pluginName).build();
	}

	
	private String getUri(){
		
		return null;
	}

}
